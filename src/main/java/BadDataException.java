
public class BadDataException extends Exception {

	private Object source;
	
	public BadDataException(Object source, String message) {
		super(message);
		this.source = source;
	}
}
