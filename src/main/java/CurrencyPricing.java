import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*
   Your class will be instantiable given the name of the CSV file. It will read the file and
   then will be ready to quote “current” rates between any two currencies by way of a
   method getFXQuote that takes two currency symbols and returns a Java double.
 */

/*
 * There may be errors in the CSV file – for example a price may be missing or
   poorly formatted. Be ready to deal with this, and test appropriately.
 */
public class CurrencyPricing {
	
	private String fileName;
	
	public CurrencyPricing(String filename) {
		this.fileName = filename;
	}
	
	public List<String[]> readData() throws IOException { 
	    List<String[]> content = new ArrayList<>();
	    try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
	        String line = "";
	        while ((line = br.readLine()) != null) {
	            content.add(line.split(","));
	        }
	    } catch (FileNotFoundException e) {
	      //Some error logging
	    }
	    return content;
	}
	
	public double getFXQuote(String currency1, String currency2) throws IOException, BadDataException {
		double res = 0.0;
		List<String[]> list = readData();
		boolean exist = false;
		
		for(int i=1; i<list.size(); i++) {
			String c1 = list.get(i)[0];
			String c2 = list.get(i)[1];
			String rate = list.get(i)[2];
			
			if( currency1.equals(c1) && currency2.equals(c2) ) {
				exist = true;
				res = Double.parseDouble(rate);
			}
		}
		
		/*
		 *    Deal with two minor currencies for which there is no direct pricing information in the file.
		 */
		if(!exist) {
			double USDtoC1 = 0.0;
			double USDtoC2 = 0.0;
			
			for(int i=1; i<list.size(); i++) {
				String c1 = list.get(i)[0];
				String c2 = list.get(i)[1];
				String rate = list.get(i)[2];
				
				if( c1.equals("USD") && c2.equals(currency1) ) {
					USDtoC1 = Double.parseDouble(rate);
				}
				
				
				if( c1.equals("USD") && c2.equals(currency2) ) {
					USDtoC2 = Double.parseDouble(rate);
				}
			}
			
			if(USDtoC1 != 0.0 && USDtoC2 != 0.0) {
				res = USDtoC2 / USDtoC1;
			} else {
				throw new BadDataException(this, "Cannot convert these two currencies!");
			}
		}
		
		return res;
	}

	public static void main(String[] args) {
		
	}

}
