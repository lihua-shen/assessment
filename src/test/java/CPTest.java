import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

public class CPTest {

	CurrencyPricing cp;
	
	@Before
	public void setUp() {
		cp = new CurrencyPricing("rates.csv");
	}
	
	@Test
	public void testCase1()
		throws Exception
	{
		assertEquals(0.1505, cp.getFXQuote("ZAR", "USD"), 0.0001);
	}
	
	@Test
	public void testCase2()
		throws Exception
	{
		assertEquals(0.1287, cp.getFXQuote("HKD", "USD"), 0.0001);
	}
	
	// Test case for two minor currencies
	@Test
	public void testCase3()
		throws Exception
	{
		assertEquals(6.6393, cp.getFXQuote("CAD", "CNY"), 0.0001);
	}
	
	// In this case, we cannot get the result
	@Test(expected=BadDataException.class)
	public void testCase4() throws Exception
	{
		assertEquals(6.6393, cp.getFXQuote("XYZ", "CNY"), 0.0001);
	}

}
